网站搭建好之后，我们往往会想要对网站的运营情况做一个大致的了解，例如今天有多少人访问，访客来自何处，浏览量最多的文章是哪一篇？等等问题。

此部分我们将会介绍网站统计的相关内容。

## 关于统计平台

虽然cloudflare和Google Adsense都提供了基础的统计功能，例如IP等，但缺乏对于具体内容和访问时间的信息，对我们优化网站访问是不利的。

国外最常用的统计平台就是Google Analytics，将在下一章节对其做详细介绍。

## 网站统计术语

**UV（Unique visitor）**
是指通过互联网访问、浏览这个网页的自然人。访问您网站的一台电脑客户端为一个访客。00:00-24:00内相同的客户端只被计算一次。

一天内同个访客多次访问仅计算一个UV。

**IP（Internet Protocol）**
独立IP是指访问过某站点的IP总数，以用户的IP地址作为统计依据。00:00-24:00内相同IP地址之被计算一次。

**UV与IP区别：**
如：你和你的家人用各自的账号在同一台电脑上登录新浪微博，则IP数+1，UV数+2。由于使用的是同一台电脑，所以IP不变，但使用的不同账号，所以UV+2


**PV（Page View）**
即页面浏览量或点击量，用户每1次对网站中的每个网页访问均被记录1个PV。用户对同一页面的多次访问，访问量累计，用以衡量网站用户访问的网页数量。

**VV（Visit View）**
用以统计所有访客1天内访问网站的次数。当访客完成所有浏览并最终关掉该网站的所有页面时便完成了一次访问，同一访客1天内可能有多次访问行为，访问次数累计。

**PV与VV区别：**
如：你今天10点钟打开了百度，访问了它的三个页面；11点钟又打开了百度，访问了它的两个页面，则PV数+5，VV数+2.
PV是指页面的浏览次数，VV是指你访问网站的次数。

Referral：https://www.zhihu.com/question/20448467

- 用户（Users）- 即访问网站的用户；可以查看选定的日期范围内有多少用户访问了网站至少一个页面。
- 报告（Reports）- Google Analytics提供超过50份免费报告，并能够创建自定义报告，帮助你分析网站数据，统计流量和记录访问者行为。数据图是常见的报告形式之一。
- 会话（Sessions）– 网站用户与网站之间的交互行为。
- 流量来源（Traffic Sources）– 显示用户是如何访问到你的网站的，访问渠道包括自然搜索（Organic Search）、社交媒体（Social）、付费广告（Paid Search）、外链引流（Referral）和直接输入网址访问（Direct）。
- 活动（Campaigns）– 跟踪用户发现网站的特定方式。比如，Google Analytics可以跟踪由Google Ads广告活动带来的流量。
- 页面浏览量（Pageviews）– 选定的日期范围内访问者浏览网站的页面总数。
- 跳出率（Bounce Rate）– 选定的日期范围内，用户只访问了网站上的一个页面，没有其他交互操作就退出访问的会话的百分比。
- 受众（Audiences）- 自定义的用户组。创建这些用户组，以帮助在谷歌分析报告、重新营销工作、谷歌广告活动和其他谷歌网站管理员工具中识别特定类型的用户。
- 转化和目标（Conversions & Goals）- 可以把用户某个交互行为定义为目标，以衡量业务价值。比如，用户完成在线购买或查看联系方式页面。转化表示网站用户完成已定义目标的次数。
- 漏斗（Funnels）— 用户完成设定目标经过的路径。

Referral：https://zhuanlan.zhihu.com/p/136378374