---
title: "Hugo：链接优化"
slug: "Hugo Slug"
description: 解决hugo默认使用标题作为网址链接导致链接中出现大量字符不便于转载的情况
date: 2021-12-13T22:05:26+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 缘起

使用`stack`主题，配置好`post`文档之后，它将我的所有文章都解析到`kermsite.com/p/文件名`这个地址下，在浏览器中，中文可以正常显示，实际意义非常明确，但是问题在于将这个链接复制到剪贴板之后，所有中文全部变成某种“乱码”（实际是`UTF-8`编码？），给我们的复制、引用造成了非常大的困难。毕竟有谁会愿意点击一个有上百个字符的不明链接呢？

## 解决方案

网址通常分为多个不同的部分：

```text
 <协议>://<主机名>/<路径>/<文件slug>?<查询字符串>#<片段>
```

路径、slug 和 查询字符串 决定了要访问服务器上的哪些内容。

需要注意的是，这三部分是**分区分大小写的**，因此，使用 "`FILE`" 与使用"`file`"将得到不同的网址。

对于静态网站来说，`slug` 就是 有效 `url` 的末尾部分的字段。

在`hugo`中 如果`front matter`中没有`slug:` 或者是为空 `slug: ""` 时，将使用 `title` 代替 `slug` ；但是如果你将 其设置为只包含空格的字符串时 `slug: " "` 的话，则最终 `url` 末尾将显示一个 `-` 。

### 自定义slug

在博客文章的头文件内添加一行`slug`即可。如本文：

```
title: "Hugo：链接优化"
slug: "Hugo Slug"
description: 解决hugo默认使用标题作为网址链接导致链接中出现大量字符不便于转载的情况
date: 2021-12-13T22:05:26+08:00
```

### 默认生成slug

如果每一次都手动添加slug实在是不方便。可以将这个设置写进默认模板里，自动生成，修改`\archetypes\post.md`相应位置为：

```
title: "{{ replace .Name "-" " " | title }}"
slug: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
```

这样你的在使用`hugo new`时就会自动将文件名作为`slug`。

> 我个人喜欢生成的时候使用全英文，然后再单独修改标题。根据你的情况进行修改吧。

## 其他作用

使用简短的`slug`据说能够增加被搜索引擎获取的能力。

## 后记

大家一定要记得在开始写博客的时候就默认生成`slug`字段啊！

不说了，我去改之前的博文去了。

## 附录

### 参考文献

1. [slug 与 url 优化 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/126112777)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

