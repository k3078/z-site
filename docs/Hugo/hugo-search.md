---
title: "Hugo：添加搜索、归档、分类"
slug: "hugo-search"
description: 为hugo-stack主题添加搜索等
date: 2021-12-06T10:53:33+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 说明

Hugo本身没有搜索功能，但许多第三方主题都内置了搜索插件。如果您并不想过多折腾搜索，可以直接下载支持搜索的第三方主题，如stack。

如果您所下载的主题不支持搜索，也可以使用第三方工具进行配置。


### Stack主题配置搜索

请参阅 [Stack主题](/Hugo/hugo-stack/) 部分。


## 其他主题配置方式

参考文献2-3

## 附录

### 参考文献

1. 
2. [给hugo添加搜索功能 | 搜百谷 (sobaigu.com)](https://sobaigu.com/hugo-set-featuer-search.html)

2. [Add Search to a Hugo site with Lunr.js and Node.js · Code with Hugo](https://codewithhugo.com/hugo-lunrjs-search-index/)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

