---
title: "Hugo：头字段&使用模板"
slug: "Hugo Template"
description: 使用一个好的模板能够大大简化我们的工作流程，尤其是对于头字段的处理
date: 2021-12-16T15:37:44+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 简介

### 头字段（FrontMatter）

应该叫做`FrontMatter`，我也不知道中文叫什么，就叫头字段吧。

就是生成的markdown文件最前面的那个家伙：
![头字段](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216154656575.png)

具体可以参考：[Front Matter | Hugo (gohugo.io)](https://gohugo.io/content-management/front-matter/)

以下是我使用的主题`stack`所支持的一些头字段。

| 字段        | 介绍                                                         | 默认值                           |
| :---------- | :----------------------------------------------------------- | :------------------------------- |
| description | 文章简介                                                     |                                  |
| image       | 特色图片                                                     |                                  |
| comments    | 显示 / 隐藏评论区                                            | `true`                           |
| license     | 文章协议 输入 `false` 可以隐藏                               | `params.article.license.default` |
| hidden      | 隐藏文章（不在首页，归档等页面显示，但是可以直接通过链接访问） | `false`                          |
| math        | 加载 KaTeX 脚本                                              |                                  |
| toc         | 显示 / 隐藏目录                                              | `params.article.toc`             |
| lastmod     | 最后更改时间                                                 |                                  |

当然，这些头字段不会要你每写一篇文章都手动添加，而是可以通过模板自动生成。

### 模板

模板文件放在`\archetypes`中，当使用`hugo`命令生成博文的时候，将自动搜索和套用模板。

例如使用

```
hugo new /post/test.md
```

就会自动套用`\archetypes\post.md`的内容，添加某些信息，并且生成`test.md`。

善用模板可以极大减轻工作量。尤其是配置头字段的工作量

## 模板示例

`post.md`

```
---
title: "{{ replace .Name "-" " " | title }}"
slug: "{{ replace .Name "-" " " | title }}"
description: "" 
date: {{ .Date }}
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["笔记"]
---

## 附录

### 参考文章

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。
```

## 附录

### 参考文章

1. [Front Matter | Hugo (gohugo.io)](https://gohugo.io/content-management/front-matter/)
2. [主题支持的 FrontMatter 字段 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/writing/supported-front-matter-fields.html)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

