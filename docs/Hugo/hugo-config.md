---
title: "Hugo：自定义博客界面"
description: ——简单介绍一下博客的配置
date: 2021-12-02T18:26:30+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 自定义网站界面

转到网站根目录对`config.yaml`或`config.toml`进行修改即可。

具体修改细则因主题不同而异。

## Hugo的文件组织形式

**注意！请将所有博文的`markdown`文件都放在`content\post`之中，再在这个文件夹里面利用子文件夹进行组织。否则Hugo不能将博文正确识别，摘要、搜索、标签等许多功能将失效。**

Hugo的一大优点是以文件夹作为博文的组织形式，非常的简洁自然，如下图所示。

```
.
└── content
    └── about
    |   └── index.md  // <- https://example.com/about/
    ├── posts
    |   ├── firstpost.md   // <- https://example.com/posts/firstpost/
    |   ├── happy
    |   |   └── ness.md  // <- https://example.com/posts/happy/ness/
    |   └── secondpost.md  // <- https://example.com/posts/secondpost/
    └── quote
        ├── first.md       // <- https://example.com/quote/first/
        └── second.md      // <- https://example.com/quote/second/
```

对于我正在使用的`stack`主题，文件最后都会被定向到`example.com/p/文件名`中，但是仍然可以使用以上的路由方式找到文件和类别。

