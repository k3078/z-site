---
title: "Hugo：插入Adsense代码"
description: "以stack主题为例"
slug: "hugo-adsense"
date: 2021-12-04T09:01:36+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

对于博主而言，写博客虽是自发性的行为，但若有经济上的反馈，自然能够极大增加热情。这里简单介绍如何在Hugo中插入谷歌广告。

## 在主题`layout`文件中加入代码

`hugo`构建网页模板在主题文件夹下面的`layout`文件夹里面，我们要在最后生成的页面的`<head>`部分加上adsense的代码，就要修改主题里面的对应部分。

直接打开`\themes\hugo-theme-stack\layouts\partials\head\custom.html`添加代码即可。

![添加代码](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211204092104430.png)

此时本地运行，就可以看到有代码了。

## 推送代码到云端（文件式安装）

直接提交修改即可。

## 推送代码到云端（submodule安装）

我是按照`submodule`的方式安装的主题。对主题进行修改后，发现不能提交，也无法推送到云端。

说明此时主模块和子模块`submodule`是两个不同的体系。应该在`hugo-theme-stack`文件夹下单独运行`git`，然后推送到云端。

![实现修改](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211204093353880.png)

![切换到子项目](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211204093022670.png)

### 使子项目和主项目匹配

完成上一步之后，子项目确实更新了，提交了一个新的版本号，但是主项目还是沿用的旧版本（这样做也是防止子项目抽风影响主项目）。此时还要转到主项目进行修改。

> 修改主项目，前面这个框要打勾，截图没截好。

![修改主项目](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211204094902747.png)

**OK！大功告成！！**

![可以看到已经有代码了](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211204095139893.png)

### 说明：权限问题

如果子模块是直接从作者处拉取的，则我们没有权限对其进行修改，所以在拉取主题文件时，就应该注意先fork再拉取自己仓库里面的版本。

如果你已经拉取了原版，可以先删除原版，再拉取自己的版本。

```
git submodule deinit --all -f 
git submodule add  -f  git://github.com/kerms0/hugo-theme-stack/ themes/hugo-theme-stack #修改成你的仓库名
```

对文件进行修改之后，回到上一步，如果提示权限问题，选择`fork`即可，会自动检测已有仓库并合并的，无需担心。


## 附录

### 参考

1. [修改主题 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/modify-theme/)
2. [Git中submodule的使用 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/87053283)
3. [GitHub怎样fork别人代码到自己仓库并进行贡献_BADAO_LIUMANG_QIZHI的博客-CSDN博客](https://blog.csdn.net/BADAO_LIUMANG_QIZHI/article/details/84297048?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2~default~CTRLIST~default-1.opensearchhbase&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2~default~CTRLIST~default-1.opensearchhbase)


