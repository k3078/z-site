---
title: "Hugo：Stack主题"
description: Stack主题，原生支持waline评论功能、搜索功能，插入视频等。
date: 2021-12-01T23:44:54+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 基本信息与参考链接



- 官网：[开始使用 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/)
- Github： [Releaseopen in new window](https://github.com/CaiJimmy/hugo-theme-stack/releases)
- 参考博文（recommended）：[Hugo Stack主题配置与使用 (bore.vip)](https://bore.vip/archives/hugo-theme-stack/)


## 演示

### Official-DEMO

[Example Site (jimmycai.com)](https://theme-stack.jimmycai.com/)

![image-20211202142116793](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211202142117444.png)


## 安装

### 方式1：手动下载

下载地址：

- https://github.com/CaiJimmy/hugo-theme-stack/archive/refs/tags/v3.5.0.zip

下载后把 `hugo-theme-stack-master` 改名为 `hugo-theme-stack` 并放到站点目录的 `theme` 文件夹下。

>  **注意！** 以这种方式安装可能会在推送到云端的时候出现错误，需要手动添加`.gitmodules`配置文件解决。请参看：[错误排查](/note/hugo/hugo-error/)

----------------------

### 方式2：Git拉取

在网站根目录打开`git bash`，输入下面命令即可：

```
git submodule add git://github.com/CaiJimmy/hugo-theme-stack/ themes/hugo-theme-stack
```

-------------------------

获取完成后，把 `exampleSite` 文件夹中的`config.yaml` 复制到站点目录下，同时删除此目录下的`config.toml`文件。其他文件也可以先复制过来，之后再删除。

## 自定义站点界面

`config.yaml`是 Hugo 站点的配置文件，已经写入了主题的可配置字段。

你可以参照本文“配置未文件示例”进行修改。

## 为文章添加题图、描述、标签等

在文章`markdown`文件对应位置添加即可，参见本文“FrontMatter示例”

> 参考：[主题支持的 FrontMatter 字段 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/writing/supported-front-matter-fields.html)

## 配置搜索功能和归档页面

首先确保在配置文件中开启了此功能。

```
widgets:
	enabled:
        - search
        - archives
        - tag-cloud
```

**然后将主题文件夹- `exampleSite`文件夹中`content\page`里的文件复制到站点目录对应的文件夹里面。**

```
├─content
│  ├─page #到这里
│  │      about.md
│  │      archives.md
│  │      search.md
└─themes
    └─hugo-theme-stack
        ├─exampleSite
        │  └─content
        │      ├─page #复制这里面的文件
        │      │      about.md
        │      │      archives.md
        │      │      search.md 
```

> 配置搜索小组件时，发现每一次使用小组件都会先跳转到搜索界面，但是不显示搜索结果，需要回车一次才会显示结果，所以之后干脆把小组件去掉了。

## 配置分类小组件

新版主题删除了（？）这一功能？

详情请参阅参考文献1

## 图标

[Tabler Icons - 800+ Highly customizable free SVG icons](https://tablericons.com/)

## 关于短代码（插入视频）

原生支持插入视频（通过短代码）。官方文档说要用本地文件，我试了一下直接用链接插入也行。

参考：https://docs.stack.jimmycai.com/zh/writing/shortcodes

## 配置文件示例

```yaml
baseurl: https://blog.kermsite.com #填你的网站地址
languageCode: zh-CN
theme: hugo-theme-stack
paginate: 5
title: KermsBlog

# Change it to your Disqus shortname before using
disqusShortname: hugo-theme-stack

# GA Tracking ID
#googleAnalytics:

# Theme i18n support
# Available values: en, fr, id, ja, ko, pt-br, zh-cn, zh-tw, es, de, nl, it, th, el, uk
DefaultContentLanguage: zh-cn  

# Set hasCJKLanguage to true if DefaultContentLanguage is in [zh-cn ja ko]
# This will make .Summary and .WordCount behave correctly for CJK languages.
hasCJKLanguage: true

permalinks:
    post: /p/:slug/
    page: /:slug/

params:
    mainSections:
        - about #设置网站首页要展示的Section，一般设成post。这样就会默认展示post文件夹里面的内容。
    featuredImageField: image
    rssFullContent: true
    favicon:

    footer:
        since: 2020
        customText:

    dateFormat:
        published: Jan 02, 2006
        lastUpdated: Jan 02, 2006 15:04 MST

    sidebar: #左边栏
        emoji: 🍥
        subtitle: 分享使知识增值
        avatar:
            enabled: true
            local: true
            src: img/avatar.jpg

    article:
        math: false
        toc: true
        readingTime: true
        license:
            enabled: false 
            default: Licensed under CC BY-NC-SA 4.0

    comments: #评论，我直接设成false
        enabled: false
        provider: disqus

        disqusjs:
            shortname:
            apiUrl:
            apiKey:
            admin:
            adminLabel:

        utterances:
            repo:
            issueTerm: pathname
            label:

        remark42:
            host:
            site:
            locale:

        vssue:
            platform:
            owner:
            repo:
            clientId:
            clientSecret:
            autoCreateIssue: false

        # Waline client configuration see: https://waline.js.org/en/reference/client.html
        waline:
            serverURL:
            lang:
            visitor:
            avatar:
            emoji:
                - https://cdn.jsdelivr.net/gh/walinejs/emojis/weibo
            requiredMeta:
                - name
                - email
                - url
            placeholder:
            locale:
                admin: Admin

        twikoo:
            envId:
            region:
            path:
            lang:

        giscus:
            repo:
            repoID:
            category:
            categoryID:
            mapping:
            lightTheme:
            darkTheme:
            reactionsEnabled: 1
            emitMetadata: 0

        gitalk:
            owner: 
            admin:  
            repo: 
            clientID: 
            clientSecret: 
        
        cusdis:
            host: 
            id: 

    widgets: #右边的小部件。注意search和archives还要进一步设置才能使用，参见前文
        enabled:
            - search
            - archives
            - tag-cloud

        archives:
            limit: 5

        tagCloud:
            limit: 10

    opengraph:
        twitter:
            # Your Twitter username
            site:

            # Available values: summary, summary_large_image
            card: summary_large_image

    defaultImage:
        opengraph:
            enabled: false
            local: false
            src:

    colorScheme:
        # Display toggle
        toggle: true

        # Available values: auto, light, dark
        default: auto

    imageProcessing:
        cover:
            enabled: true
        content:
            enabled: true

### Custom menu
### See https://docs.stack.jimmycai.com/configuration/custom-menu.html
### To remove about, archive and search page menu item, remove `menu` field from their FrontMatter
menu: 
    main:
        - identifier: home
          name: 首页&关于
          url: /
          weight: -100 #权重
          params:
              ### For demonstration purpose, the home link will be open in a new tab
              newTab: false
              icon: home #更多的图标信息见后
        
        - identifier: share
          name: 资源信息分享
          url: /share/ #链接，文件夹的相对位置即可。
          weight: -90
          params:
              ### For demonstration purpose, the home link will be open in a new tab
              newTab: false
              icon: hash

        - identifier: summary
          name: 资源信息总结
          url: /summary/
          weight: -70
          params:
              ### For demonstration purpose, the home link will be open in a new tab
              newTab: false
              icon: hash

        - identifier: note
          name: 学习&笔记
          url: /note/
          weight: -80
          params:
              ### For demonstration purpose, the home link will be open in a new tab
              newTab: false
              icon: messages


        - identifier: soft
          name: 软件&网站
          url: /soft/
          weight: -60
          params:
              ### For demonstration purpose, the home link will be open in a new tab
              newTab: false
              icon: messages

    # social:
    #     - identifier: github
    #       name: GitHub
    #       url: https://github.com/CaiJimmy/hugo-theme-stack
    #       params:
    #         icon: brand-github
            
    #     - identifier: twitter
    #       name: Twitter
    #       url: https://twitter.com
    #       params:
    #         icon: brand-twitter

related:
    includeNewer: true
    threshold: 60
    toLower: false
    indices:
        - name: tags
          weight: 100

        - name: categories
          weight: 200

markup:
    goldmark:
        renderer:
            ## Set to true if you have HTML content inside Markdown
            unsafe: false
    tableOfContents:
        endLevel: 4
        ordered: true
        startLevel: 2
    highlight:
        noClasses: false

```

## FrontMatter示例

```
title: "在线编程平台（IDE）"
description: ""
date: 2021-12-02T13:42:48+08:00
image: https://test.jpg
math: 
license: 
hidden: false
comments: false
draft: true	
categories: [""]
tags: ["总结","免费","IDE"]
```

## 附注

### 参考文献

1. [hugo主题stack (yinhe.co)](https://yinhe.co/archives/20210401_hugo_theme_stack.html) 更为详细的配置
2. [介绍 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/) 官方文件
3. [Hugo Stack主题配置与使用 (bore.vip)](https://bore.vip/archives/hugo-theme-stack/) 
