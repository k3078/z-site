---
title: "Hugo：为博客插入视频（shortcodes）"
description: 利用shortcodes功能为博客插入在线视频、哔哩哔哩、youtube等
slug: "hugo shortcodes"
date: 2021-12-11T22:30:16+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

直接在markdown文章中插入HTML代码以自定义页面元素，往往会导致无法识别。Hugo对此的解决方案是短代码（`shortcodes`）。

我们平时使用这类静态网站构建工具时，编辑的文档是`markdown`格式的，但是最后渲染是`html`格式。虽然`markdown`便于书写和预览，可我们向网页中添加元素也不如纯`html`方便了。

而短代码，就是为了解决这个问题而生的。

当你在`markdown`中插入一段短代码，hugo构建时会自动搜寻对应文件夹下的模板，将关键字改写之后自动插入生成的网页中。

只需要提前配置好模板，几乎可以方便的插入任何元素。此外，许多主题实际已经内置了许多模板，只要调用即可。

## 模板

在 `md` 文件中嵌入 `ShortCodes` 时，程序会先查找你的项目的根目录下的 `/layouts/shortcodes` 文件夹下的模板文件，再查找 `theme` 对应主题目录下的 `/layouts/shortcodes` 文件夹。

推荐先看一下主题里面有什么，像我用的`stack`主题内置了B站、腾讯、油管、视频四个短代码。我想要插入的时候直接调用即可。

如果主题里面没有你想要的，也可以自己新建。

例如我使用的`video`短代码，则体现为在`shortcodes`目录下的一个`video.html`文件，内容为:

```html
{{- $src := .Get "src" | default (.Get 0) -}}
<div class="video-wrapper">
    <video
    controls
    src="{{- $src -}}"
    {{ with .Get "poster" }}poster="{{- . -}}"{{ end }}
    {{ with .Get "autoplay" }}autoplay{{ end }}
    >
        <p>
            Your browser doesn't support HTML5 video. Here is a
            <a href="{{- $src -}}">link to the video</a> instead.
        </p>
    </video>
</div>

```

## 调用

在`markdown`文件里调用短代码也非常方便。只需给定几个参量。例如调用一个视频，来源是同一目录下的`video.mp4`，设置成自动播放，海报为`video-poster.png`。当然，个人还是推荐视频单独上传，不走服务器流量，也是节省空间。直接将`src`改成对应视频链接即可。

## 附录

### 参考文献

1. [Hugo 通过 ShortCodes 添加视频 | Kays Blog ⑅︎◡̈︎* (gitee.io)](https://caymanhk.gitee.io/posts/006_hugo通过shortcodes添加视频/)
2. [Hugo的shortcode(支持视频等)和主题修改Li.027 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/396330042)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

