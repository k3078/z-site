---
title: "hugo：添加评论功能（Waline）"
description: 折腾了一个上午。原来是主题文件里面有一个小BUG
date: 2021-12-06T08:18:44+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false	
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

## 缘起

在浏览其他博客时，发现有很多过时的信息，由于没有评论系统，博主也收不到反馈，导致垃圾信息长期挂在那里，不断误导后来人。

最近也一直在做信息的收集。这种工作最注重时效性。故开一个评论功能也是十分必要的。

Hugo本身不支持评论，需要通过插件支持。我使用的stack主题，可以直接配置waline（？）。~~二者都有非常详细的文档。所以这里也只是稍稍提及。~~

~~**淦我按照官方文档的指引配置居然不显示，弄了一上午最后手动添加的**。。。~~

**淦我晓得为什么不显示了！！！！！作者我\*\*\***

## 配置waline

虽然步骤有点多，好在全过程都有详细文档。

参照[快速上手 | Waline](https://waline.js.org/guide/get-started.html#leancloud-设置-数据库)一路配置过来就可以了，只需要完成配置数据库和服务端的部分。

> 第一步注册leancloud的链接已经失效，需要手动注册

## 自动添加到Hugo中（未成功）

理论上说只要如下修改`config.yaml`即可：

```yaml
    #开启comments功能，设置为waline
    comments:
        enabled: true
        provider: waline
        
        # 以及配置waline
        waline:
            serverURL: https://waline-pc25lo4ye-yw2667899.vercel.app/ #这里填上一步生成的站点
            lang: zh-CN
            visitor: false
            avatar: 
            emoji:
                - https://cdn.jsdelivr.net/gh/walinejs/emojis/weibo
            requiredMeta:
                - name
                - email
                - url
            placeholder:
            locale:
                admin: Kerm
```

就可以了。

> 但是我配置完成之后却并没有显示，这是为什么呢？

## 这是为什么呢?!——解决方案

通过我一个上午的仔细的研究，大致了解了Hugo的渲染方式。凭借着我孜孜不倦的分析，终于找到了在`themes\hugo-theme-stack\layouts_default\single.html`这个文件里的这样一段代码：

```html
{{ if not (eq .Params.comments false) }}
    {{ partial "comments/include" . }}
{{ end }}
```

以及类似的一段代码：

```
on-phone--column {{ if .Site.Params.widgets.enabled }}extended{{ else }}compact{{ end }}
```

大家发现了什么吗。。。

第一段的意思就是如果`.Params.comment`这个参数不等于`false`，就调用`comment`部件进行渲染。

第二段，类似的做法，但是这里采用的参数是`.Site.Params.widgets.enabled`。

后者有一个`.Site`的前缀，前者没有。通过分析，我发现带前缀的参数最后是读取的根目录下，我们之前修改的那个文件。**而不带前缀的，我至今不知道它在读取什么玩意儿。**

此时，你只需要稍微修改一下：

```
{{ if not (eq .Site.Params.comments false) }}
    {{ partial "comments/include" . }}
{{ end }}
```

**就\*\*的可以显示了！！！！！！！！**

**啊啊啊啊！！！！作者我\*\*\*！！！！**

![效果](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211206152311497.png)

## 推送到云端

由于这里我们对hugo的主题代码进行修改了，如果你是用`submodule`进行配置的话，需要单独对主题仓库进行同步才能在云端显示。具体可以参考：

[在Hugo中插入Adsense代码 (kermsite.com)](https://kermsite.com/p/在hugo中插入adsense代码/)

第3、4部分。

## 手动添加（已废弃）

~~只能手动添加了。~~实际上也还是比较方便的。

> 我还是写一下吧。

直接转到`\themes\hugo-theme-stack\layouts\partials\comments\provider\waline.html`，全部复制，然后转到`themes\hugo-theme-stack\layouts_default\single.html`，用刚刚复制的代码替代掉上一部分提及的有问题的代码块即可。

修改后的效果：

```html
{{ define "body-class" }}
    {{ $TOCEnabled := default (default false .Site.Params.article.toc) .Params.toc }}
    {{- .Scratch.Set "hasTOC" (and (ge (len .TableOfContents) 100) $TOCEnabled) -}}
    article-page {{ if (.Scratch.Get "hasTOC") }}has-toc{{ end }}
{{ end }}

{{ define "container-class" }}
    {{ if (.Scratch.Get "hasTOC") }}
        extended
    {{ else }}
        on-phone--column {{ if .Site.Params.widgets.enabled }}extended{{ else }}compact{{ end }}
    {{ end }}
{{ end }}

{{ define "main" }}
    {{ partial "article/article.html" . }}

    {{ partial "article/components/related-contents" . }}
     
    <!-- 修改从这里开始 -->
    
    <script src='//cdn.jsdelivr.net/npm/@waline/client/dist/Waline.min.js'></script>
    <div id="waline" class="waline-container"></div>
    <style>
        .waline-container {
            background-color: var(--card-background);
            border-radius: var(--card-border-radius);
            box-shadow: var(--shadow-l1);
            padding: var(--card-padding);
        }
        .waline-container .vcount {
            color: var(--card-text-color-main);
        }
    </style>
    
    {{- with .Site.Params.comments.waline -}}
    {{- $config := dict "el" "#waline" "dark" `html[data-scheme="dark"]` -}}
    {{- $replaceKeys := dict "serverurl" "serverURL" "requiredmeta" "requiredMeta" "wordlimit" "wordLimit" "pagesize" "pageSize" "avatarcdn" "avatarCDN" "avatarforce" "avatarForce" -}}
    
    {{- range $key, $val := . -}}
        {{- if $val -}}  
            {{- $replaceKey := index $replaceKeys $key -}}
            {{- $k := default $key $replaceKey -}}
    
            {{- $config = merge $config (dict $k $val) -}}
        {{- end -}}
    {{- end -}}
    
    <script>
        new Waline({{ $config | jsonify | safeJS }});
    </script>
    {{- end -}}

    <!-- 到这里结束 -->

    {{ partialCached "footer/footer" . }}

    {{ partialCached "article/components/photoswipe" . }}
{{ end }}

{{ define "left-sidebar" }}
    {{ if (.Scratch.Get "hasTOC") }}
        <div id="article-toolbar">
            <a href="{{ .Site.BaseURL | relLangURL }}" class="back-home">
                {{ (resources.Get "icons/back.svg").Content | safeHTML }}
                <span>{{ T "article.back" }}</span>
            </a>
        </div>
    {{ else }}
        {{ partial "sidebar/left.html" . }}
    {{ end }}
{{ end }}

{{ define "right-sidebar" }}
    {{ if (.Scratch.Get "hasTOC") }}
        <aside class="sidebar right-sidebar sticky">
            <section class="widget archives">
                <div class="widget-icon">
                    {{ partial "helper/icon" "hash" }}
                </div>
                <h2 class="widget-title section-title">{{ T "article.tableOfContents" }}</h2>
                
                <div class="widget--toc">
                    {{ .TableOfContents }}
                </div>
            </section>
        </aside>
    {{ end }}
{{ end }}
```

## 效果演示

![image-20211206084858508](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211206084858508.png)

## 添加评论推送

waline原生支持评论推送，参照官方文档开启即可。

## 附录

### 参考文献

1. [快速上手 | Waline](https://waline.js.org/guide/get-started.html#html-引入-客户端)
2. [hugo-theme-stack/single.html at master · CaiJimmy/hugo-theme-stack (github.com)](https://github.com/CaiJimmy/hugo-theme-stack/blob/master/layouts/_default/single.html)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

