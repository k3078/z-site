---
title: "Markdown：语法简介"
slug: "intro-md"
description: 语法简介，插入视频，以及编辑器介绍
date: 2021-12-11T23:13:06+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["笔记","博客搭建"]
---

## 说明

现在常见的静态博客工具所使用的编辑方式都是markdown了。所以还是有必要学习一下的。

> 例如，hugo生成的文章就是`\\content\\post\\***.md"`

## 编辑器

推荐使用`Typora`。虽然已经转向收费软件，但仍然可以使用开发版。目前我所使用的就是`0.11.13`：

![我已出仓，感觉良好](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211212233001600.png)

下载地址：[Typora for windows — beta version release](https://typora.io/windows/dev_release.html)

当然也欢迎大家支持发行版，毕竟也不是特别的贵（非正当途径就算了。。。）：[Typora — a markdown editor, markdown reader.](https://typora.io/)

> 推荐理由：所见即所得。通常的markdown软件有两栏，无法直接看到编辑结果，但是typora在当你完成输入之后就能实时反馈结果。和word类似的效果，用惯了比word更方便。

## 基本语法

如果你使用`typora`，也可以用快捷键进行替代。

> 参考[2020Typora小白完全使用教程 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/293557841)

本部分来自：[Markdown 语法快速入门手册_w3cschool](https://www.w3cschool.cn/markdownyfsm/markdownyfsm-odm6256r.html)

### Headers 标题：

```
#  H1
##  H2
###  H3
####  H4
#####  H5
######  H6
```

另外，H1和H2还能用以下方式显示：

```
一级标题
===
 
二级标题
---
```

### Emphasis 文本强调：

```
*斜体* or _强调_
**加粗** or __加粗__
***粗斜体*** or ___粗斜体__
```

但是，如果你的`*`和`_`两边都有空白的话，它们就只会被当成普通的符号：这是一段`*文本强调*`的说明示例。

如果要在文字前后直接插入普通的星号或底线，你可以用反斜线（转义符）：`\*这是一段被星号包围的文字\*`

### Lists 列表：

Unordered 无序列表：

```
* 无序列表
* 子项
* 子项
 
+ 无序列表
+ 子项
+ 子项
 
- 无序列表
- 子项
- 子项
```

Ordered 有序列表：

```
1. 第一行
2. 第二行
3. 第三行
 
1. 第一行
- 第二行
- 第三行
```

组合：

```
* 产品介绍（子项无项目符号）
    此时子项，要以一个制表符或者4个空格缩进
 
* 产品特点
    1. 特点1
    - 特点2
    - 特点3
* 产品功能
    1. 功能1
    - 功能2
    - 功能3
 
```

可有时我们会出现这样的情况，首行内容是以日期或数字开头：`2017. 公司年度目标。`

为了避免也被转化成有序列表，我们可以在"."前加上反斜杠（转义符）：`2017\. 公司年度目标。`

### Links 连接（title为可选项）：

Inline-style 内嵌方式：

```
[W3Cschool](http://www.w3cschool.cn/ "W3Cschool")
```

Reference-style 引用方式：

```
[链接文字][id]
[id]: http://www.w3cschool.cn/ "标题文字"
```

Relative reference to a repository file 引用存储文件：

```
[链接文字](../path/file/readme.text "标题文字")
```

还能这样使用：

```
[链接文字][]
[链接文字]: http://www.w3cschool.cn/
```

Email 邮件：

```
<example@w3cschool.cn>
```

### Images 图片：

Inline-style 内嵌方式：

```
![替代文字](http://statics.w3cschool.cn/images/w3c/index-logo.png "标题文字")
```

Reference-style 引用方式：

```
![替代文字][logo]
[logo]: http://statics.w3cschool.cn/images/w3c/index-logo.png "标题文字"
```

### Code and Syntax Highlighting 代码和语法高亮：

**标记一小段行内代码：**

```
本文是一篇介绍`Markdown`的语法的文章
```

如果高亮的内容包含```号，可以这样写：

```
`` `包裹起来` ``
```

**语法高亮：**

````
```html
    <div>Syntax Highlighting</div>
```
```css
    body{font-size:12px}
```
```javascript
    var s = "JavaScript syntax highlighting";
    alert(s);
```
```php
    <?php
      echo "hello, world!";
    ?>
```
```python
    s = "Python syntax highlighting"
    print s
```
````

### Block Code 代码分组(代码区块)：

在该行开头缩进4个空格或一个制表符(tab)

**Blockquotes 引用：**

```
> Email-style angle brackets
> are used for blockquotes.
> > And, they can be nested.
> #### Headers in blockquotes
> * You can quote a list.
> * Etc.
```

### Hard Line Breaks 换行：

```
在一行的结尾处加上2个或2个以上的空格，也可以使用</br>标签
第一行文字，
第二行文字
```

### Horizontal Rules 水平分割线：

```
***
* * *
- - -
```

### Escape character 转义符(反斜杠)：

Markdown 可以利用反斜杠来插入一些在语法中有其它意义的符号，例如：如果你想要用星号加在文字旁边的方式来做出强调效果，你可以在星号的前面加上反斜杠：

```
\*literal asterisks\*
```

Markdown 支持以下这些符号前面加上反斜杠来帮助插入普通的符号：

```
\反斜杠  
`反引号  
*星号  
_下划线  
{}花括号  
[]方括号  
()括弧  
#井字号  
+加号  
-减号  
.英文句 
!感叹号
```

## 进阶语法

由于`markdown`支持插入`html`语句，理论上可以插入任何内容。**但是不一定每一个软件都能够正确显示html内容。**

例如插入视频代码在`typora`里面能显示视频，传到`hugo`就直接一堆代码糊脸了。

> `hugo`也能插入视频，但是需要使用`shortcodes`，参考：[Hugo：短代码（shortcodes） (kermsite.com)](https://kermsite.com/p/hugo短代码shortcodes/)

### 插入视频

html中的[video标签](https://www.zhihu.com/search?q=video标签&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"article"%2C"sourceId"%3A350887044})

```html
<!-- mp4格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="mp4" src="mp4格式视频" type="video/mp4">
</videos>

<!-- webm格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="webm" src="webm格式视频" type="video/webm">
</videos>

<!-- ovg格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="ogv" src="ogv格式视频" type="video/ogv">
</videos>
```

### 插入（视频）小组件

html中的iframe标签

```html
<iframe 
src="视频或者网页路径" 
scrolling="no" 
border="0" 
frameborder="no" 
framespacing="0" 
allowfullscreen="true" 
height=600 
width=800> 
</iframe>
<!-- 相当于是子网页 -->
<!-- B站分享链接提供 -->
```

## 关于图片的插入

Hugo是支持直接插入和引用本地图片的，有多种方式。我本人推荐使用Page Bundles，比较方便，并且其他markdown编辑器也能够识别。只需要新建页面时：

``` powershell
hugo new /post/new.md
#改成
hugo new /post/new/index.md
```

之后在`new`这个文件夹下添加图片图片，并直接引用文件名即可。VScode上有一键实现复制+引用的插件，可以参考[这篇文章](https://kermsite.com/p/vscode-markdown/)

目录结构：

``` txt
+---测试文章
|       cover.jpg
|       Design-V1.jpg
|       Design-V2.jpg
|       index.md
```

index.md文件内容：

``` markdown
---
title: "测试文章"
description: "文章简介"
date: "2020-08-10 01:00:00+0200"
slug: "test-post"
image: "cover.jpg"
categories:
    - 博客
tags:
    - Hugo
    - Stack
---

![图片 1](Design-V1.jpg)   

![图片 2](Design-V2.jpg)
```

也还有其他方法，不过我不太推荐，参考附录文章5-6。

## 附录

### 参考文献

1. [Markdown中插入视频 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/350887044)
2. [2020Typora小白完全使用教程 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/293557841)
3. [Markdown 语法快速入门手册_w3cschool](https://www.w3cschool.cn/markdownyfsm/markdownyfsm-odm6256r.html)
4. https://docs.stack.jimmycai.com
5. https://www.iterdaily.com/post/hugo_add_image_into_post/
6. https://blog.csdn.net/perfumekristy/article/details/122086009
7. https://www.rectcircle.cn/series/hugo/content-management/page-bundles/

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

