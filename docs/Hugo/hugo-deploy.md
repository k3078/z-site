---
title: "Hugo：部署至互联网"
description: 使用Github和Netlify将你的博客发布到互联网
slug: "hugo-deploy"
date: 2021-12-01T23:44:48+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

**本文使用Netlify进行演示**

不同静态托管平台，所使用的Hugo版本也略有不同。Netlify使用的是extend版本。某些主题，例如hugo-stack和hugo-book，只能在netlify平台使用。

## 上传本地文件到Github

在网站根目录新建一个Git仓库，推送到Github即可。

### 注册GitHub

这个应该不要多说了吧

官网：[GitHub](https://github.com/)

> 如果你是国内直接连接GitHub，访问不了也是非常正常的现象，可以参照[Github：加速方案汇总 (kermsite.com)](https://blog.kermsite.com/p/fastergithub/)，下载工具以访问。

### 下载和安装git

官网：[Git - Downloads (git-scm.com)](https://git-scm.com/downloads)

> 下载可能有点慢。

[![如图所示](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211202163901646.png)](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211202163901646.png)

Git安装时有很多步骤，里面会让你进行非常多的设置。没**有特殊情况我们就直接全部保持默认，一路`next`就好了**。

完成之后会在右键菜单里面添加`Git Bash`和`Git GUI`。

### 初始化仓库

在网站根目录，用`Git Bash`运行

```
git init    #初始化仓库
git add .   #将所有文件添加到仓库里
```

> 说明：接下来我会转到GitHub desktop进行操作，当然也可以继续用Git，但是个人感觉git链接GitHub的过程有些复杂，需要配置密钥什么的，用GitHub自家的软件只需要登录就可以了。

### 下载和安装GitHub desktop

首先访问链接下载：

- [GitHub Desktop | Simple collaboration from your desktop](https://desktop.github.com/)

![官网](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211116223530980.png)

安装完成之后，选择使用Github登录：![登录界面](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211116223525407.png)

### 将仓库推送到云端

转到Github Desktop，`add local repository`，选择网站所在根目录，提交更改，并推送到云端。

> 更多可以参照：[Github Desktop：友好的图形化Git客户端 (kermsite.com)](https://blog.kermsite.com/p/github-desktop/)

添加仓库，这一步会要求设定一个云端的仓库名称，可以和你的文件夹名称不一样，不过你得记住这个名字，之后还要使用。

![添加仓库](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216145930219.png)

推送修改到云。

![我的已经配置好了，初始化会有一些差别，但是流程一样](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216145729122.png)

## 将Netlify连接到Github

netlify是一个静态网站托管平台，适合用来搭建个人博客什么的，本站就是托管在netlify上。

官网（注册）：https://app.netlify.com/ 

注册时直接使用邮箱会比较好，保护隐私嘛。`outlook`可以用。

注册完成，转到后台，新建一个网站。

![选择导入已有仓库](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216151256658.png)

从`Github`部署：

![GitHub](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216151252335.png)

选择你想要部署的仓库（选择你的博客仓库，就是刚才GitHubDesktop上传时使用的仓库名称）：

![选择你想要部署的仓库](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216151530067.png)

检查部署信息，点击部署即可：

![我这里是hugo的部署参数](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216151608631.png)

等待部署完成，转到控制台，netlify会自动给你分配一个二级域名，使用这个域名就可以访问你的博客了。

![我这里用的是之前的一张图](https://cdn.jsdelivr.net/gh/kerms0/pics/img/image-20211216151848966.png)

## 附录

### 参考文章

1. [Git：安装和使用 (kermsite.com)](https://blog.kermsite.com/p/git安装和使用/)

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

