1. [Quick Start | Hugo (gohugo.io)](https://gohugo.io/getting-started/quick-start/)
2. [Hugo Stack主题配置与使用 (bore.vip)](https://bore.vip/archives/hugo-theme-stack/)
3. [介绍 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/)
4. [Hugo 中文帮助文档 (aiaide.com)](https://hugo.aiaide.com/) 这份帮助文档非常全面，大家可以直接使用这个。
5. [Hugo | 为 Blog 增加评论区 (mantyke.icu)](https://mantyke.icu/2021/comment/)
6. [开始使用 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/)
7. [Releaseopen in new window](https://github.com/CaiJimmy/hugo-theme-stack/releases)
8. [Hugo Stack主题配置与使用 (bore.vip)](https://bore.vip/archives/hugo-theme-stack/)

