---
title: "Hugo：本地安装"
description: ——在Windows系统安装Hugo
date: 2021-12-01T22:47:33+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["博客搭建"]
---


## 安装软件

首先到官网下载Windows版：[Releases · gohugoio/hugo (github.com)](https://github.com/gohugoio/hugo/releases)

下好之后找个空文件夹放了，比如我这里用的`D:\myblog`。

## 配置环境变量

修改系统的环境变量，将`hugo.exe`所在文件夹添加到里面，如图所示。**修改完成后，需要重启计算机**。

> `win+s`组合键打开搜索框，输入**环境变量**就可以找到了。

![image-20211213000025496](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211213000025496.png)

## 生成站点文件

打开终端

> 在文件夹里面，按住`Shift`同时右键单击，就可以看到`在此处打开命令提示符`或`在Windows终端中打开`

输入`hugo version`如果有输出说明安装ok。

![image-20211121113133755](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121113133755.png)

转到你准备存放博客的文件夹：

```
hugo new site myblog #myblog设成你想要的博客名
```

![image-20211121113148148](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121113148148.png)

有这样的输出说明已经安装好博客了，不过现在还没有内容，也没有主题，我们稍微设置一下。

## 配置Hugo主题-以Stack为例

和其他静态博客生成程序略有不同的是，Hugo必须配置一个主题才能访问，否则就是一片空白。我这里使用的是`slack`主题。

<!-- >注意：您所下载的主题，和您的博客文件本身，会被识别为两个单独的项目。 -->

### 文件式安装（推荐）

下载地址：

- https://github.com/CaiJimmy/hugo-theme-stack/archive/refs/tags/v3.5.0.zip

下载后把 `hugo-theme-stack-master` 改名为 `hugo-theme-stack` 并放到站点目录的 `theme` 文件夹下。删掉里面所有带`git`字眼的文件和文件夹。

>  这种方式等于是把主题文件中的git库删除了，你也可以保留，不过以这种方式安装可能会在推送到云端的时候出现错误，需要手动添加`.gitmodules`配置文件解决。请参看：[错误排查](/note/hugo/hugo-error/)

----------------------

### submodule安装

> 如果你需要对主题文件进行单独的配置，例如修改模板等，推荐你先把主题文件fork到自己的仓库里，然后将下面的代码换成自己的仓库。

在网站根目录打开`git bash`，输入下面命令即可：

```
# 使用官方仓库
git submodule add git://github.com/CaiJimmy/hugo-theme-stack/ themes/hugo-theme-stack

# 使用自己的仓库，先fork，以我的仓库为例
git submodule add git://github.com/kerms0/hugo-theme-stack/ themes/hugo-theme-stack
```

----------------------------

获取完成后，把 `exampleSite` 文件夹中的`config.yaml` 复制到站点目录下。后者是 Hugo 站点的配置文件，已经写入了主题的可配置字段。删除站点目录下的`config.toml`文件。

然后为了之后写博客方便，可以把模板也复制过来，将`\themes\hugo-theme-stack\archetypes`复制到`\archetypes`中，进行替换。

## 本地构建

使用

```
hugo server
```

就可以在本地运行服务了：

![image-20211121181820499](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121181820499.png)

转到`http://localhost:1313/`：

![image-20211121181801730](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121181801730.png)

## 新建页面

使用

```
hugo new post/test.md
```

创建页面。也可以不加`post`，直接输入文件名，实际上都是添加到`content`文件夹里面的。

![image-20211121182127088](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121182127088.png)

![image-20211121182138985](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121182138985.png)

使用markdown编辑软件对文件进行编辑，值得注意的一点是如果要发布这篇文章，一定要将`draft`设为`false`，否则不会对其进行生成（你也可以在`\archetypes`中直接把它默认为`true`）

再次访问网站，可以看到我们的页面已经添加上了：

![image-20211121182551751](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211121182551751.png)



