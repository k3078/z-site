---
title: "Hugo：常见错误排查"
slug: "hugo-error"
description: ——希望大家永远不要点开这个页面
date: 2021-12-01T23:17:02+08:00
image: 
math: 
license: 
hidden: false
comments: false
draft: false
categories: ["学习&笔记"]
tags: ["博客搭建"]
---

这里主要归集在使用过程中遇到的错误以及解决方法，供参考。
## 安装主题后，推送到云端出现`gitmodules`错误
在站点根目录下新建`.gitmodules`如下配置（以`stack`主题为例）

```
[submodule "themes/hugo-theme-stack"]
	path = themes/hugo-theme-stack
	url = https://github.com/CaiJimmy/hugo-theme-stack/
```

## 文章不显示

这里也有几种可能性。

1. 在`markdown`文件里设置了`draft: true`。软件会判定为草稿不构建。
2. `hugo server`的实时更新功能只对已有文件修改有效，创建新文件不会渲染。重启`server`就可以了
3. 主题设置有误。例如

```
params:
  mainSections:
	- post
```

​	这样默认主页是渲染你放在`content\post`文件夹里面的文件，这时如果你新建文章新建到`posts`文件夹里面当然不会显示了

## 没有搜索栏

这不是一个BUG，Hugo本来就不支持搜索。

可以通过[Algolia](https://www.algolia.com/)添加搜索功能，也有部分主题自带搜索功能，例如本博客使用的`stack`。

> 关于stack添加搜索，请参照：[Hugo：Stack主题](/note/hugo/hugo-stack/)
>
> 亦可参考：[给hugo添加搜索功能 | 搜百谷 (sobaigu.com)](https://sobaigu.com/hugo-set-featuer-search.html)
>
> 以及：[Add Search to a Hugo site with Lunr.js and Node.js · Code with Hugo](https://codewithhugo.com/hugo-lunrjs-search-index/)

## 分类不匹配

这也是在使用中发现的。`stack`主题是可以自定义分类的描述的。但是实际使用中出了问题，产生了一个新的空白的分类，对不上。

解决方案：需要将目录名称对应为实际网站打开的链接`categories\分类实际名称`中的名称新建文件夹。

主要是在分类名称中有特殊字符时出现。
