---
title: "typecho路过图床插件"
slug: "Typechoimg"
date: 2022-02-16T14:53:26+08:00
draft: false
description: ""
categories: [""]
tags: ["博客搭建","typecho"]
---

如图，在`admin/footer.php`中，添加如下代码即可

    <script async id="chevereto-pup-src" src="https://imgse.com/sdk/pup.js" data-url="https://imgse.com/upload" data-auto-insert="markdown-embed"></script>

[![hBSR0g.png](https://z3.ax1x.com/2021/09/01/hBSR0g.png)](https://imgtu.com/i/hBSR0g)

刷新后台即可看见如下按钮，支持本地上传和链接

[![h0zdGq.png](https://z3.ax1x.com/2021/09/01/h0zdGq.png)](https://imgtu.com/i/h0zdGq)

当然最方便的还是本地typora设置自动上传插件然后写好之后复制到typecho。