### 安装

可以直接参考官方文档：[Install — Caddy Documentation (caddyserver.com)](https://caddyserver.com/docs/install#debian-ubuntu-raspbian)

**ubuntu**

```
sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
sudo apt update
sudo apt install caddy
```

> **注意：我连续两次使用都出现了复制之后只执行第一行的情况，所以如果发现没安装上，检查一下是否完全执行了所有命令**

**centos**


````
yum install yum-plugin-copr 

yum copr enable @caddy/caddy 

yum install caddy
````

安装完成之后，直接访问服务器域名，即可看到欢迎页面（确保已经开放80端口）

### 常用命令

```bash
# 启动
systemctl start caddy
# 停止
systemctl stop caddy
# 重启
systecmtl restart caddy
# 查看日志和服务状态 （以上三个命令，都可以用 service caddy XXX 的方式实现）
service caddy status
# 载入配置文件（在caddyfile所在目录执行）
caddy reload
```

### 配置文件

Caddy默认的配置文件为`/etc/caddy/Caddyfile`，默认配置如下：

```txt
# The Caddyfile is an easy way to configure your Caddy web server.
#
# Unless the file starts with a global options block, the first
# uncommented line is always the address of your site.
#
# To use your own domain name (with automatic HTTPS), first make
# sure your domain's A/AAAA DNS records are properly pointed to
# this machine's public IP, then replace ":80" below with your
# domain name.

:80 {
	# Set this path to your site's directory.
	root * /usr/share/caddy

	# Enable the static file server.
	file_server

	# Another common task is to set up a reverse proxy:
	# reverse_proxy localhost:8080

	# Or serve a PHP site through php-fpm:
	# php_fastcgi localhost:9000
}

# Refer to the Caddy docs for more information:
# https://caddyserver.com/docs/caddyfile
```

- `:80`定义了一个站点，监听来自80端口的所有请求
- `root * /usr/share/caddy`定义了站点根目录
- `file_server`表示静态文件服务器，允许访问站点根目录下的文件

## 搭建反向代理

```txt
xxx.kermsite.com {
    encode gzip
    reverse_proxy localhost:8000
}
```

1.  `encode gzip`表示对响应启用`Gzip`压缩
2.  `reverse_proxy localhost:8000`表示反代本地的`8000`端口
2.  如果使用cloudflare托管域名，请在最初设置DNS时不要点亮小云朵。否则强制`https`很可能出现无法访问情况。

## 搭建静态网站

如下，配置文件：

```
XXX.kermsite.com {
      root * /usr/local/ServerStatus/web
      encode gzip
      file_server
}

```

### 注意事项

1. `root * /usr/local/ServerStatus/web`表明将根目录指向你的网站文件所在目录。默认访问此目录中`index.html`文件。
2. 必须写`file_server`这行，否则搭建好之后访问为空页面。

## 自动获取证书

Caddy支持自动配置SSL证书。只需提供一个邮箱即可。

```
XXX.kermsite.com {
        root * /usr/share/www
        tls kerm@kermsite.com
        file_server
}
```

1. 如果使用cloudflare托管域名，请在最初设置DNS时不要点亮小云朵。否则无法生成证书。
2. 如果使用cloudflare托管域名，配置证书后显示重定向过多，可以在cloudflare TLS处选择严格模式。