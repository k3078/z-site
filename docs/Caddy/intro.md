### 简介

> Caddy is a powerful, extensible platform to serve your sites, services, and apps, written in Go. If you're new to Caddy, the way you serve the Web is about to change.
>
> Caddy是一个强大、可拓展的平台，用于托管你的网站、服务和应用，用GO语言编写。如果你刚开始使用Caddy，你托管网站的方式即将改变。

Caddy是一个非常容易配置的Web服务端。支持很多拓展功能，**原生支持`https`证书自动安装。**

如果你刚开始建站，还处在使用宝塔面板的阶段，并且希望更进一步的了解网站配置，那么caddy可以作为你的下一步学习的对象。

此外，由于caddy舍去了图形界面，其资源占用要比宝塔面板更少。对于某些性能孱弱的服务器，或者某些只需执行单一功能，部署单一网站的服务器，是非常合适的（例如Hax）。

官方文档：[Welcome — Caddy Documentation (caddyserver.com)](https://caddyserver.com/docs/)

