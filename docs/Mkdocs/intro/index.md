MkDocs 是一个用于创建项目文档的 **快速, 简单 , 完美华丽 的静态站点生成器**. 文档源码使用 Markdown 来撰写, 用一个 YAML 文件作为配置文档，基于python开发。

本教程将简单介绍MKdocs的安装、使用和美化。帮助您从零开始打造自己的文档界面。除 Mkdocs 本身外，还用到一个漂亮的主题 Material for MkDocs，其官方文档分别如下：

- MkDocs：[MkDocs](https://www.mkdocs.org/)
- Material for MkDocs：[Getting started - Material for MkDocs (squidfunk.github.io)](https://squidfunk.github.io/mkdocs-material/getting-started/)

DEMO：

- 本文档。
- VPS使用指南：[VPS使用指南 (zhelper.net)](https://use.zhelper.net/)。