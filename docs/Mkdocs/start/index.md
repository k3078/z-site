Mkdocs基于python开发，使用python module的形式进行安装。请首先保证安装有python，然后运行

```
# 只下载Mkdocs（使用默认主题，比较丑）
pip install mkdocs
# 或者 一次性下载主题+MKdocs（推荐）
pip install mkdocs-material
```

以下使用主题+Mkdocs

## 新建站点和本地运行

转到一个空文件夹下，使用`mkdocs new .`新建站点，将会生成以下文件：

```
.
├─ docs/
│  └─ index.md
└─ mkdocs.yml
```

其中，`mkdocs.yml`是站点的配置文件，`docs`文件夹下是我们的文档内容。访问网站主页，默认是展示`docs/index.md`中的内容。

本地运行，可以使用`mkdocs serve `。

## 添加内容

直接在`docs`文件夹下新建Markdown文件即可。关于Markdown，可以参考https://markdown.com.cn/basic-syntax/。

## 站点配置

使用目录下的`mkdocs.yml`对网站进行配置，相较于其他静态网站生成工具，MKdocs的配置也很简单。下面是一个配置示例，更加详细的内容可以参考[Changing the colors - Material for MkDocs (squidfunk.github.io)](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/)。

```YAML
# 站点名称
site_name: VPS使用指南
# 主题配置
theme:
  # 使用Material主题
  name: material
  # 修改模板内容，用于插入广告、评论系统等，非必要
  # custom_dir: overrides
  language: zh
  # 其他功能选项。这里开启intergrate是为了将目录移到左边。
  # features:
  #   - toc.integrate


# 导航。如果不设置导航，Mkdocs将按照默认顺序对docs文件夹中文件生成导航
# 所有文档放在docs文件夹中，这里只用写相对路径。结尾应当是md
# nav:
#   - 挂机回血:
#     - GTI国内: profit/gti.md
#     - Income国外: profit/income.md
#     - Peer2profit国外: profit/peer2profit.md
#   - 编程开发:
#     - Jupyter: dev/jupyterwindows/index.md
#     - VscodeRemote远程开发: dev/vscoderemote/index.md
#     # - VscodeWeb版: dev
#   - 运行维护:
#     - 哪吒面板: monitor/neza/index.md
#     - Serverstatus: monitor/serverstatus/index.md
#     - UptimeRobot: monitor/uptimerobot/index.md
#     - VNC虚拟桌面: monitor/dockervnc/index.md
#   - 下载工具:
#     - Aira2+RSS+Onedrive+油猴: download/aria2.md 
#   - 日常使用:
#     - RSS阅读器 TTRSS: daily/ttrss/index.md
#     - 稍后读 Wallabag: daily/wallabag.md
#     - Emby 媒体库: daily/emby.md
#     - 弹弹play 动漫: daily/dandanplay.md
#   - NPS 内网穿透: other/nps/index.md
#   - Webdav 本地文件分享: other/webdav/index.md
#   - Sharelist 网盘列表: other/sharelist.md
#   - 个人博客/文档: other/blog.md
#   - VPN: other/vpn.md
```