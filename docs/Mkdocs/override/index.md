除了在`mkdocs.yml`进行自定义之外，我们还可以通过 override 的形式实现充分的自定义，例如添加广告和评论模块等。

在`mkdocs.yml`中，开启overrides功能，并在站点目录下，新建文件夹`overrides`。

```YAML
theme:
  name: material
  custom_dir: overrides
```

文件结构：

```
.
├─ overrides/
# 需要自定义的界面
│  └─ main.html
└─ mkdocs.yml

```

注意：如果您只需要在某一页面插入某一 html 代码，例如 iframe 等。可以直接将此代码写入 md 文件。其原理是Markdown原生支持html语法。不过有时候也会出BUG，可以先尝试再做调节。

## 添加广告

此部分介绍添加Google AdSense广告。

在overrides下新建`base.html`文件，写入以下内容（注意修改Adsense代码部分），保存即可。

```html
{#-
  This file was automatically generated - do not edit
-#}
{% import "partials/language.html" as lang with context %}
<!doctype html>
<html lang="{{ lang.t('language') }}" class="no-js">
  <head>
    {% block site_meta %}
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      {% if page and page.meta and page.meta.description %}
        <meta name="description" content="{{ page.meta.description }}">
      {% elif config.site_description %}
        <meta name="description" content="{{ config.site_description }}">
      {% endif %}
      {% if page and page.meta and page.meta.author %}
        <meta name="author" content="{{ page.meta.author }}">
      {% elif config.site_author %}
        <meta name="author" content="{{ config.site_author }}">
      {% endif %}
      {% if page.canonical_url %}
        <link rel="canonical" href="{{ page.canonical_url }}">
      {% endif %}
      <link rel="icon" href="{{ config.theme.favicon | url }}">
      <meta name="generator" content="mkdocs-{{ mkdocs_version }}, mkdocs-material-8.2.8">
    {% endblock %}
    {% block htmltitle %}
      {% if page and page.meta and page.meta.title %}
        <title>{{ page.meta.title }} - {{ config.site_name }}</title>
      {% elif page and page.title and not page.is_homepage %}
        <title>{{ page.title | striptags }} - {{ config.site_name }}</title>
      {% else %}
        <title>{{ config.site_name }}</title>
      {% endif %}
    {% endblock %}
    {% block styles %}
      <link rel="stylesheet" href="{{ 'assets/stylesheets/main.644de097.min.css' | url }}">
      {% if config.theme.palette %}
        {% set palette = config.theme.palette %}
        <link rel="stylesheet" href="{{ 'assets/stylesheets/palette.e6a45f82.min.css' | url }}">
        {% if palette.primary %}
          {% import "partials/palette.html" as map %}
          {% set primary = map.primary(
            palette.primary | replace(" ", "-") | lower
          ) %}
          <meta name="theme-color" content="{{ primary }}">
        {% endif %}
      {% endif %}
    {% endblock %}
    {% block libs %}{% endblock %}
    {% block fonts %}
      {% if config.theme.font != false %}
        {% set text = config.theme.font.text | d("Roboto", true) %}
        {% set code = config.theme.font.code | d("Roboto Mono", true) %}
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family={{
            text | replace(' ', '+') + ':300,300i,400,400i,700,700i%7C' +
            code | replace(' ', '+') + ':400,400i,700,700i'
          }}&display=fallback">
        <style>:root{--md-text-font:"{{ text }}";--md-code-font:"{{ code }}"}</style>
      {% endif %}
    {% endblock %}
    {% for path in config["extra_css"] %}
      <link rel="stylesheet" href="{{ path | url }}">
    {% endfor %}
    {% include "partials/javascripts/base.html" %}
    {% block analytics %}
      {% include "partials/integrations/analytics.html" %}
    {% endblock %}
    {% block extrahead %}{% endblock %}
    <script async="" src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-7692266966719684" crossorigin="anonymous"></script>
  </head>
  {% set direction = config.theme.direction or lang.t('direction') %}
  {% if config.theme.palette %}
    {% set palette = config.theme.palette %}
    {% if not palette is mapping %}
      {% set palette = palette | first %}
    {% endif %}
    {% set scheme  = palette.scheme  | replace(" ", "-") | lower %}
    {% set primary = palette.primary | replace(" ", "-") | lower %}
    {% set accent  = palette.accent  | replace(" ", "-") | lower %}
    <body dir="{{ direction }}" data-md-color-scheme="{{ scheme }}" data-md-color-primary="{{ primary }}" data-md-color-accent="{{ accent }}">
  {% else %}
    <body dir="{{ direction }}">
  {% endif %}
    {% set features = config.theme.features or [] %}
    {% if not config.theme.palette is mapping %}
      {% include "partials/javascripts/palette.html" %}
    {% endif %}
    <input class="md-toggle" data-md-toggle="drawer" type="checkbox" id="__drawer" autocomplete="off">
    <input class="md-toggle" data-md-toggle="search" type="checkbox" id="__search" autocomplete="off">
    <label class="md-overlay" for="__drawer"></label>
    <div data-md-component="skip">
      {% if page.toc | first is defined %}
        {% set skip = page.toc | first %}
        <a href="{{ skip.url | url }}" class="md-skip">
          {{ lang.t('skip.link.title') }}
        </a>
      {% endif %}
    </div>
    <div data-md-component="announce">
      {% if self.announce() %}
        <aside class="md-banner">
          <div class="md-banner__inner md-grid md-typeset">
            {% block announce %}{% endblock %}
          </div>
        </aside>
      {% endif %}
    </div>
    {% if config.extra.version %}
      <div data-md-component="outdated" hidden>
        <aside class="md-banner md-banner--warning">
          {% if self.outdated() %}
            <div class="md-banner__inner md-grid md-typeset">
              {% block outdated %}{% endblock %}
            </div>
            {% include "partials/javascripts/outdated.html" %}
          {% endif %}
        </aside>
      </div>
    {% endif %}
    {% block header %}
      {% include "partials/header.html" %}
    {% endblock %}
    <div class="md-container" data-md-component="container">
      {% block hero %}{% endblock %}
      {% block tabs %}
        {% if not "navigation.tabs.sticky" in features %}
          {% if "navigation.tabs" in features %}
            {% include "partials/tabs.html" %}
          {% endif %}
        {% endif %}
      {% endblock %}
      <main class="md-main" data-md-component="main">
        <div class="md-main__inner md-grid">
          {% block site_nav %}
            {% if nav %}
              {% if page and page.meta and page.meta.hide %}
                {% set hidden = "hidden" if "navigation" in page.meta.hide %}
              {% endif %}
              <div class="md-sidebar md-sidebar--primary" data-md-component="sidebar" data-md-type="navigation" {{ hidden }}>
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    {% include "partials/nav.html" %}
                  </div>
                </div>
              </div>
            {% endif %}
            {% if page.toc and not "toc.integrate" in features %}
              {% if page and page.meta and page.meta.hide %}
                {% set hidden = "hidden" if "toc" in page.meta.hide %}
              {% endif %}
              <div class="md-sidebar md-sidebar--secondary" data-md-component="sidebar" data-md-type="toc" {{ hidden }}>
                <div class="md-sidebar__scrollwrap">
                  <div class="md-sidebar__inner">
                    {% include "partials/toc.html" %}
                  </div>
                </div>
              </div>
            {% endif %}
          {% endblock %}
          <div class="md-content" data-md-component="content">
            <article class="md-content__inner md-typeset">
              {% block content %}
                {% include "partials/content.html" %}
              {% endblock %}
            </article>
          </div>
        </div>
        {% if "navigation.top" in features %}
          <a href="#" class="md-top md-icon" data-md-component="top" data-md-state="hidden">
            {% include ".icons/material/arrow-up.svg" %}
            {{ lang.t('top.title') }}
          </a>
        {% endif %}
      </main>
      {% block footer %}
        {% include "partials/footer.html" %}
      {% endblock %}
    </div>
    <div class="md-dialog" data-md-component="dialog">
      <div class="md-dialog__inner md-typeset"></div>
    </div>
    {% block config %}
      {%- set app = {
        "base": base_url,
        "features": features,
        "translations": {},
        "search": "assets/javascripts/workers/search.5e67fbfe.min.js" | url
      } -%}
      {%- if config.extra.version -%}
        {%- set _ = app.update({ "version": config.extra.version }) -%}
      {%- endif -%}
      {%- set translations = app.translations -%}
      {%- for key in [
        "clipboard.copy",
        "clipboard.copied",
        "search.config.lang",
        "search.config.pipeline",
        "search.config.separator",
        "search.placeholder",
        "search.result.placeholder",
        "search.result.none",
        "search.result.one",
        "search.result.other",
        "search.result.more.one",
        "search.result.more.other",
        "search.result.term.missing",
        "select.version.title"
      ] -%}
        {%- set _ = translations.update({ key: lang.t(key) }) -%}
      {%- endfor -%}
      <script id="__config" type="application/json">
        {{- app | tojson -}}
      </script>
    {% endblock %}
    {% block scripts %}
      <script src="{{ 'assets/javascripts/bundle.c44cc438.min.js' | url }}"></script>
      {% for path in config["extra_javascript"] %}
        <script src="{{ path | url }}"></script>
      {% endfor %}
    {% endblock %}
  </body>
</html>
```


## 添加评论模块

Mkdocs-Material默认支持Giscus评论系统，官方也给出了教程。不过仍然需要手动配置一下。

Giscus是一个基于GitHub Discussion系统的评论模块，用户可以使用GitHub账号登录，支持markdown格式评论，界面简洁美观。DEMO：[VPS使用指南 (zhelper.net)](https://use.zhelper.net/)。

本部分可以参考：[Adding a comment system - Material for MkDocs (squidfunk.github.io)](https://squidfunk.github.io/mkdocs-material/setup/adding-a-comment-system/?h=comment)

1. 首先在GitHub上安装GitcusAPP：[GitHub Apps - giscus](https://github.com/apps/giscus)
2. 新建一个仓库，用于存放评论，配置仓库：Settings-General-Features，打开Discussions功能。
3. 转到[giscus](https://giscus.app/zh-CN)，按照指示一步步生成代码。
4. 在overrides下新建`main.html`文件，写入以下内容即可。

```html
{% extends "base.html" %}

{% block content %}
  {{ super() }}
  <!-- Giscus -->
  <h2 id="__comments">{{ lang.t("meta.comments") }}</h2>
  
    
  <!-- 这里改成你的Giscus代码 -->
  <script src="https://giscus.app/client.js"
  data-repo="zhelper-net/comment"
  data-repo-id="R_kgDOHN5v-w"
  data-category="Announcements"
  data-category-id="DIC_kwDOHN5v-84COt3H"
  data-mapping="url"
  data-reactions-enabled="1"
  data-emit-metadata="0"
  data-input-position="top"
  data-theme="light"
  data-lang="zh-CN"
  data-loading="lazy"
  crossorigin="anonymous"
  async>
</script>



  <!-- Reload on palette change -->
  <script>
    var palette = __md_get("__palette")
    if (palette && typeof palette.color === "object")
      if (palette.color.scheme === "slate") {
        var giscus = document.querySelector("script[src*=giscus]")
        giscus.setAttribute("data-theme", "dark") 
      }

    /* Register event handlers after documented loaded */
    document.addEventListener("DOMContentLoaded", function() {
      var ref = document.querySelector("[data-md-component=palette]")
      ref.addEventListener("change", function() {
        var palette = __md_get("__palette")
        if (palette && typeof palette.color === "object") {
          var theme = palette.color.scheme === "slate" ? "dark" : "light"

          /* Instruct Giscus to change theme */
          var frame = document.querySelector(".giscus-frame")
          frame.contentWindow.postMessage(
            { giscus: { setConfig: { theme } } },
            "https://giscus.app"
          )
        }
      })
    })
  </script>
{% endblock %}
```



## 关于
 Mkdocs