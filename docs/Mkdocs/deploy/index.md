MKdocs可以很方便地部署到Vercel，以及其他静态托管服务，当然也包括服务器。

## 部署到Vercel

在GitHub/gitlab上新建项目，上传你的站点文件。

在项目文件夹下，新建`package.json`，写入以下内容：

```json
{
    "name": "mkdocs",
    "version": "1.0.0",
    "private": true,
    "scripts": {
        "dev": "mkdocs serve",
        "build": "mkdocs build -d public"
    }
}
```

在项目文件夹下，新建`requirements.txt`，写入以下内容：

```
mkdocs-material==8.2.8
mkdocs
```

文件结构：

```
.
├─ package.json
├─ requirements.txt
└─ mkdocs.yml
```

之后在vercel使用默认配置部署即可。详情